import React from 'react';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import GrainIcon from '@material-ui/icons/Grain';
import CloudIcon from '@material-ui/icons/Cloud';

const useStyles = makeStyles((theme) => ({
    cityName: {
        marginTop: '1%',
        color: '#ebeff5',
        // marginRight: '1%',
        textTransform: 'capitalize'
    },
    temp: {
        marginBottom: '1%',
        fontFamily: 'Roboto',
        color: '#ebeff5',
    },

    mood: {
        color: '#ebeff5'
    },
    icon: {
        marginRight: '5%',
        marginBottom: '5%'
    },
    background: {
        width: theme.spacing(22),
        height: theme.spacing(28),
        paddingTop: '10%',
        margin: 'auto',
        marginTop: '2%',
        borderRadius: '5px',
        background: 'linear-gradient(transparent, rgba(74, 74, 74, 0.5))',
    },
}));


const DailyWeatherItem = (props) => {
    const classes = useStyles();

    return (
        <div>

            <div className={classes.background}>
                <Typography className={classes.cityName} variant="h6" component="h6" gutterBottom align='center' >
                    {props.dailyWeather.date}
                    {/* Sunday */}
                </Typography>
                <div>
                    <Typography className={classes.temp} align='center' variant='h4'>
                        {props.dailyWeather.mood === 'Cloudy' ? <CloudIcon className={classes.icon} fontSize="large" />
                            : props.dailyWeather.rainy ? <GrainIcon className={classes.icon} fontSize="large" />
                                : <WbSunnyIcon className={classes.icon} fontSize="large" />} <br></br>
                        {Math.round(props.dailyWeather.tempMax)}
                        -
                        {Math.round(props.dailyWeather.tempMin)}°

                    </Typography>
                </div>
                <Typography variant='h6' className={classes.mood} align='center'>
                    {props.dailyWeather.mood}
                    {/* Sunny */}
                </Typography>
            </div>
        </div>
    )
}


export default DailyWeatherItem;