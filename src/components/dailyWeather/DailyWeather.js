import React from 'react';
import DailyWeatherItem from './DailyWeatherItem';
import Typography from '@material-ui/core/Typography';




class DailyWeather extends React.Component {

    getStyleMainDiv = () => {
        return {
            marginTop: '3%'
        }
    }

    getStyleDailyItem = () => {
        return {
            display: 'flex',
            flexFlow: 'row',
            justifyContent: 'space-evenly',

        }
    }
    getStyleDailyWord = () => {
        return {
            marginLeft: '5%',
            marginBottom: '1%',
            color: '#ebeff5',
            fontFamily: 'Roboto',
        }
    }

    render() {
        return (
            <div style={this.getStyleMainDiv()}>
                <Typography variant='h4' style={this.getStyleDailyWord()}>
                    Daily
                </Typography>
                <div style={this.getStyleDailyItem()}>
                    {this.props.dailyWeatherTest.map((dailyWeather) => (
                        <DailyWeatherItem key={dailyWeather.id} dailyWeather={dailyWeather} />
                    ))}
                </div>
            </div>
        )


    };


}


export default DailyWeather;

