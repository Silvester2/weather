import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    sunny: {
        backgroundImage: 'url("/sunny.jpg")',
    },
    cloudy: {
        backgroundImage: 'url("/cloudy.jpg")',
    },
    background: {
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundSize: ' cover',
        backgroundAttachment: 'fixed',
        height: "100vh"
    }
}));

const Body = (props) => {
    const classes = useStyles();
    const backgroundImage = props.todayWeather.rainy || props.todayWeather.mood === 'Cloudy' ? classes.cloudy : classes.sunny

    return (
        <div className={[classes.background, backgroundImage].join(" ")}  >
            {props.children}
        </div>
    )
}

export default Body;