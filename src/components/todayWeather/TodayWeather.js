import React from 'react';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import GrainIcon from '@material-ui/icons/Grain';
import CloudIcon from '@material-ui/icons/Cloud';



const useStyles = makeStyles((theme) => ({
    cityName: {
        marginTop: '2%',
        color: '#ebeff5',
        marginRight: '1%',
        textTransform: 'capitalize'
    },
    temp: {
        marginBottom: '1%',
        fontFamily: 'Roboto',
        color: '#ebeff5',
    },

    mood: {
        color: '#ebeff5'
    },
    icon: {
        marginRight: '5%',
        marginBottom: '10%'
    },
    background: {
        width: theme.spacing(22),
        height: theme.spacing(28),
        paddingTop: '1.8%',
        margin: 'auto',
        marginTop: '3%',
        borderRadius: '5px',
        background: 'linear-gradient(transparent, rgba(74, 74, 74, 0.5))',
    },
}));


const TodayWeather = (props) => {
    const classes = useStyles();

    return (

        <div className={classes.background}>
            <Typography className={classes.cityName} variant="h4" component="h4" gutterBottom align='center' >
                {props.todayWeather.city}
                {/* Tirana */}
            </Typography>
            <div>
                <Typography className={classes.temp} align='center' variant='h2'>
                    {props.todayWeather.mood === 'Cloudy' ? <CloudIcon className={classes.icon} fontSize="large" />
                        : props.todayWeather.rainy ? <GrainIcon className={classes.icon} fontSize="large" />
                            : <WbSunnyIcon className={classes.icon} fontSize="large" />}
                    {Math.round(props.todayWeather.weather)}
                    {/* 30° */}

                </Typography>
            </div>
            <Typography variant='h5' className={classes.mood} align='center'>
                {props.todayWeather.mood}
                {/* Mostly Sunny */}
            </Typography>
        </div>

    )
}



export default TodayWeather;