import React from 'react';
import Body from './components/body/Body'
import Header from './components/header/Header';
import TodayWeather from './components/todayWeather/TodayWeather';
import DailyWeather from './components/dailyWeather/DailyWeather';
import axios from 'axios'
import { v4 as uuid } from 'uuid';
import './App.css';



class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cityName: '',
      selectedCityName: 'Tirana',
      cityKey: '',
      todayWeather: {},
      dailyWeather: [],
      dailyWeatherTest: [
        // {
        //   id: uuid(),
        //   date: '11-07-2020',
        //   mood: 'Mostly Sunny',
        //   rainy: false,
        //   tempMax: '30°',
        //   tempMin: '22°'
        // }
        // ,
        // {
        //   id: uuid(),
        //   date: '11-07-2020',
        //   mood: 'Mostly Sunny',
        //   rainy: false,
        //   tempMax: '30°',
        //   tempMin: '22°'
        // },
        // {
        //   id: uuid(),
        //   date: '11-07-2020',
        //   mood: 'Mostly Sunny',
        //   rainy: false,
        //   tempMax: '30°',
        //   tempMin: '22°'
        // },
        // {
        //   id: uuid(),
        //   date: '11-07-2020',
        //   mood: 'Mostly Sunny',
        //   rainy: false,
        //   tempMax: '30°',
        //   tempMin: '22°'
        // },
        // {
        //   id: uuid(),
        //   date: '11-07-2020',
        //   mood: 'Mostly Sunny',
        //   rainy: false,
        //   tempMax: '30°',
        //   tempMin: '22°'
        // }
      ],
      hourlyWeather: [],
    }

  }

  // Api Key: ivwQqrc1l7WlNC8JAL8yenfUAyXAsiHr
  // Api alternative Key: MwD20ovXvaidrbx7hYR3uXdTNq39BSno

  getCityKey = () => {
    const cityName = this.state.selectedCityName !== '' ? this.state.selectedCityName : 'Tirana'
    return axios.get(`http://dataservice.accuweather.com/locations/v1/cities/search?apikey=ivwQqrc1l7WlNC8JAL8yenfUAyXAsiHr&q=${cityName}&language=en-us`)
      .then(res => {
        let cityKey = ''
        if (res.data.length > 0) {
          cityKey = res.data[0].Key
        }

        return cityKey
      })
  }

  getTodayWeather = (cityKey) => {
    cityKey = cityKey !== '' ? cityKey : '6522'
    return axios.get(`http://dataservice.accuweather.com/forecasts/v1/hourly/1hour/${cityKey}?apikey=ivwQqrc1l7WlNC8JAL8yenfUAyXAsiHr&metric=true`)
      .then(res => {
        console.log(res)
        let rainy = 'PrecipitationType' in res.data[0]
        let todayWeather = {
          city: this.state.selectedCityName,
          mood: res.data[0].IconPhrase,
          rainy: rainy,
          weather: res.data[0].Temperature.Value,
        }
        return todayWeather

      })

  }

  getDailyWeather = (cityKey) => {
    cityKey = cityKey !== '' ? cityKey : '6522'
    return axios.get(`http://dataservice.accuweather.com/forecasts/v1/daily/5day/${cityKey}?apikey=ivwQqrc1l7WlNC8JAL8yenfUAyXAsiHr&metric=true`)
      .then(res => {
        console.log(res)

        let dailyWeather = res.data.DailyForecasts.map((el) => {
          let date = el.Date.split('T')
          let rainy = 'PrecipitationType' in el.Day
          let currentDayWeather = {
            id: uuid(),
            date: date[0],
            mood: el.Day.IconPhrase,
            rainy: rainy,
            tempMax: el.Temperature.Maximum.Value,
            tempMin: el.Temperature.Minimum.Value
          }
          return currentDayWeather

        })
        console.log(dailyWeather)
        return dailyWeather


      })
  }

  getHourlyWeather = (cityKey) => {
    cityKey = cityKey !== '' ? cityKey : '6522'
    return axios.get(`http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/${cityKey}?apikey=ivwQqrc1l7WlNC8JAL8yenfUAyXAsiHr&metric=true`)
      .then(res => {

        let hourlyWeather = res.data.map((el) => {
          let currentHourlyWeather = {
            id: uuid(),
            date: el.DateTime,
            mood: el.IconPhrase,
            temp: el.Temperature.Value,
            units: 'C'
          }

          return currentHourlyWeather
        })

        return hourlyWeather
      })
  }



  async componentDidMount() {
    console.log(this.state.dailyWeatherTest)
    // Request for cityKey
    const cityKey = await this.getCityKey();

    // Request Api after city key
    const [today, daily, hourly] = await Promise.all([
      this.getTodayWeather(cityKey), this.getDailyWeather(cityKey), this.getHourlyWeather(cityKey)
    ]);

    this.setState({
      cityKey: cityKey,
      todayWeather: today,
      dailyWeather: daily,
      hourlyWeather: hourly,
    })

  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedCityName !== this.state.selectedCityName) {
      // Request for cityKey
      const cityKey = await this.getCityKey();

      // Request Api after city key
      const [today, daily, hourly] = await Promise.all([
        this.getTodayWeather(cityKey), this.getDailyWeather(cityKey), this.getHourlyWeather(cityKey)
      ]);

      this.setState({
        cityKey: cityKey,
        todayWeather: today,
        dailyWeather: daily,
        hourlyWeather: hourly,
      })
    }
  }

  onChange = (e) => {
    this.setState({ cityName: e.target.value })

  }

  onSubmit = (e) => {
    e.preventDefault()
    this.setState({ selectedCityName: this.state.cityName })
  }

  render() {
    return (
      <div className="App">
        <Body todayWeather={this.state.todayWeather}>
          <Header onChange={this.onChange} onSubmit={this.onSubmit} selectedCityName={this.state.selectedCityName}>
            <TodayWeather cityName={this.state.cityName} todayWeather={this.state.todayWeather} />
            <DailyWeather dailyWeatherTest={this.state.dailyWeather} />
          </Header>
        </Body>
      </div>
    );
  }

}
export default App;
